// const person: {
//   name: string;
//   age: number;
// } = {
const person = {
  name: 'Maximilian',
  age: 30
};

console.log(person.name);

console.log(person.nickname); // TS throws error

