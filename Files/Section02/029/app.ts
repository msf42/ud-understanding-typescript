let userInput: unknown;
let userName: string;

userInput = 5; // allowed
userInput = 'Max'; // allowed

// we can't say username = userInput without type checking
if (typeof userInput === 'string') {
  userName = userInput;
}

// never = never returns a value; not just void, but never
function generateError(message: string, code: number): never {
  throw { message: message, errorCode: code };
  // while (true) {}
}

generateError('An error occurred!', 500);