// best not to explicitly set return type
// let TS do its job
function add(n1: number, n2: number): number {
  return n1 + n2;
}

// this return type is `void`
// just means the function doesn't return anything
function printResult(num: number) {
  console.log('Result: ' + num)
}

printResult(add(5, 12)) // 17

let someValue: undefined; // is a valid type
// but a fuction is not allowed to return undefined
// void makes it clear that a function has no return value

// let combineValues: Function;  // the function type

let combineValues: (a: number, b: number) => number; // accepts any function that takes two values and returns a number

combineValues = add;

console.log(combineValues(8,8)) // 16