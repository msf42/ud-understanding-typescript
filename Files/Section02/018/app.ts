const person: {
  name: string;
  age: number;
  hobbies: string[];
  role: [number, string]; // this makes it a tuple
} = {
  name: 'Maximilian',
  age: 30,
  hobbies: ['Sports', 'Cooking'],
  role: [2, 'author']
};
