# Getting Started

---

## 001: Welcome

Done

---

## 002: What is TypeScript & Why Should You Use It?

![image](images/Lesson002.png)

![image](images/Lesson002b.png)

---

## 003: Installing & Using TypeScript

- the compiled JS may not look that different than you would have normally written
- TypeScript forces you to write cleaner JavaScript
- Below:
  - `!` means this will always return a value
  - `as HTMLInputElement` tells the source of the value
  - `num1: number` give the type

```ts
const button = document.querySelector("button");
const input1 = document.getElementById("num1")! as HTMLInputElement;
const input2 = document.getElementById("num2")! as HTMLInputElement;

function add(num1: number, num2: number) {
  return num1 + num2;
}

button.addEventListener("click", function() {
  console.log(add(+input1.value, +input2.value));
});
```

### using-ts.js (the compiled JS file)

```js
var button = document.querySelector("button");
var input1 = document.getElementById("num1");
var input2 = document.getElementById("num2");
function add(num1, num2) {
    return num1 + num2;
}
button.addEventListener("click", function () {
    console.log(add(+input1.value, +input2.value));
});

```

---

## 004: TypeScript Advantages - Overview

![image](images/Lesson005.png)

---

## 005: Course Outline

![image](images/Lesson006.png)

---

## 006: How to Get the Most Out of the Course

![image](images/Lesson007.png)

---

## 007: Setting Up a Code Editor / IDE

- already set up

---

## 008: The Course Project Setup

- just set up the course folder

---
