# TypeScript Basics & Basic Types

---

## 009: Module Introduction

- Done

---

## 010: Using Types

### Core Types

- same as JS
  - number
  - string
  - boolean

### app.ts

```ts
function add(n1: number, n2: number) {
  return n1 + n2;
}

const number1 = 5;
const number2 = 2.8;

const result = add(number1, number2);
console.log(result);
```

### app.js

```js
function add(n1, n2) {
    return n1 + n2;
}
var number1 = 5;
var number2 = 2.8;
var result = add(number1, number2);
console.log(result);
```

---

## 011: TypeScript Types vs JavaScript Types

### app.ts

```ts
function add(n1: number, n2: number) {
  // if (typeof n1 !== 'number' || typeof n2 !== 'number') {
  //   throw new Error('Incorrect input!');
  // }
  return n1 + n2;
}

const number1 = 5;
const number2 = 2.8;

const result = add(number1, number2);
console.log(result);
```

### app.js

```js
function add(n1, n2) {
    // if (typeof n1 !== 'number' || typeof n2 !== 'number') {
    //   throw new Error('Incorrect input!');
    // }
    return n1 + n2;
}
var number1 = 5;
var number2 = 2.8;
var result = add(number1, number2);
console.log(result);
```

---

## 012: Important: Type Casing

- core primitive types in TypeScript are all lower case

---

## 013: Working with Numbers, Strings, & Booleans

### app.ts

```ts
function add(n1: number, n2: number, showResult: boolean, phrase: string) {
  // if (typeof n1 !== 'number' || typeof n2 !== 'number') {
  //   throw new Error('Incorrect input!');
  // }
  const result = n1 + n2;
  if (showResult) {
    console.log(phrase + result);
  } else {
    return result;
  }
}

const number1 = 5; // 5.0
const number2 = 2.8;
const printResult = true;
const resultPhrase = 'Result is: ';

add(number1, number2, printResult, resultPhrase);
```

---

## 014: Type Assignment & Type Inference

- TypeScript infers types
  - if a variable is given a type, TS determines it should keep that type

---

## Quiz 1: Understanding Types

- Done

---

## 015: Object Types

- TypeScript's representation of the object (in image and commented code)

```ts
// const person: {
//   name: string;
//   age: number;
// } = {
const person = {
  name: 'Maximilian',
  age: 30
};

console.log(person.name);

console.log(person.nickname); // TS throws error
```

![image](images/Lesson015.png)

---

## 016: Nested Objects & Types

Of course object types can also be created for nested objects.

Let's say you have this JavaScript object:

```ts
const product = {
  id: 'abc1',
  price: 12.99,
  tags: ['great-offer', 'hot-and-new'],
  details: {
    title: 'Red Carpet',
    description: 'A great carpet - almost brand-new!'
  }
}
```

This would be the type of such an object:

```ts
{
  id: string;
  price: number;
  tags: string[],
  details: {
    title: string;
    description: string;
  }
}
```

---

## 017: Arrays Types

### app.ts

```ts
// const person: {
//   name: string;
//   age: number;
// } = {
const person = {
  name: 'Maximilian',
  age: 30,
  hobbies: ['Sports', 'Cooking']
};

let favoriteActivities: string[];
// if necessary, we can use any[], but this loses the advantages of TS
favoriteActivities = ['Sports'];

console.log(person.name);

for (const hobby of person.hobbies) {
  console.log(hobby.toUpperCase());
  // console.log(hobby.map()); // Error, because it is a string, not an array
}
```

---

## 018: Working with Tuples

- push is still allowed in tuples

### app.ts

```ts
const person: {
  name: string;
  age: number;
  hobbies: string[];
  role: [number, string]; // this makes it a tuple
} = {
  name: 'Maximilian',
  age: 30,
  hobbies: ['Sports', 'Cooking'],
  role: [2, 'author']
};
```

---

## 019: Working with Enums

- an enum defines a set of related constants
- TypeScript is making more sense to me now that I see this

![image](images/Lesson019.png)

### app.ts

```ts
enum Role { ADMIN = 'ADMIN', READ_ONLY = 100, AUTHOR = 'AUTHOR' };

const person = {
  name: 'Maximilian',
  age: 30,
  hobbies: ['Sports', 'Cooking'],
  role: Role.ADMIN
};


let favoriteActivities: string[];
favoriteActivities = ['Sports'];

console.log(person.name);

for (const hobby of person.hobbies) {
  console.log(hobby.toUpperCase());
  // console.log(hobby.map()); // !!! ERROR !!!
}

if (person.role === Role.AUTHOR) {
  console.log('is author');
}
```

### app.js

```js
var Role;
(function (Role) {
  Role["ADMIN"] = "ADMIN";
  Role[Role["READ_ONLY"] = 100] = "READ_ONLY";
  Role["AUTHOR"] = "AUTHOR";
})(Role || (Role = {}));
;
var person = {
  name: 'Maximilian',
  age: 30,
  hobbies: ['Sports', 'Cooking'],
  role: Role.ADMIN
};

var favoriteActivities;
favoriteActivities = ['Sports'];
console.log(person.name);
for (var _i = 0, _a = person.hobbies; _i < _a.length; _i++) {
  var hobby = _a[_i];
  console.log(hobby.toUpperCase());
  // console.log(hobby.map()); // !!! ERROR !!!
}
if (person.role === Role.AUTHOR) {
  console.log('is author');
}

```

---

## 020: The "any" Type

- doesn't tell TS anything

---

## 021: Union Types

### app.ts

```ts
// union type, such as => number | string

function combine(input1: number | string, input2: number | string) {
  let result;
  if (typeof input1 === 'number' && typeof input2 === 'number') {
    result = input1 + input2;
  } else {
    result = input1.toString() + input2.toString();
  }
  return result;
}

const combinedAges = combine(30, 26);
console.log(combinedAges);

const combinedNames = combine('Max', 'Anna');
console.log(combinedNames);
```

---

## 022: Literal Types

### app.ts

```ts
function combine(
  input1: number | string,
  input2: number | string,
  resultConversion: 'as-number' | 'as-text'
) {
  let result;
  if (typeof input1 === 'number' && typeof input2 === 'number' || resultConversion === 'as-number') {
    result = +input1 + +input2;
  } else {
    result = input1.toString() + input2.toString();
  }
  return result;
  // if (resultConversion === 'as-number') {
  //   return +result;
  // } else {
  //   return result.toString();
  // }
}

const combinedAges = combine(30, 26, 'as-number');
console.log(combinedAges); // 56

const combinedStringAges = combine('30', '26', 'as-number');
console.log(combinedStringAges); // 56

const combinedNames = combine('Max', 'Anna', 'as-text');
console.log(combinedNames); // MaxAnna
```

### app.js

```js
function combine(input1, input2, resultConversion) {
  var result;
  if (typeof input1 === 'number' && typeof input2 === 'number' || resultConversion === 'as-number') {
    result = +input1 + +input2;
  }
  else {
    result = input1.toString() + input2.toString();
  }
  return result;
  // if (resultConversion === 'as-number') {
  //   return +result;
  // } else {
  //   return result.toString();
  // }
}
var combinedAges = combine(30, 26, 'as-number');
console.log(combinedAges); // 56
var combinedStringAges = combine('30', '26', 'as-number');
console.log(combinedStringAges); // 56
var combinedNames = combine('Max', 'Anna', 'as-text');
console.log(combinedNames); // MaxAnna
```

---

## 023: Type Aliases / Custom Types

- allows us to create our own custom types, usually from unions

### app.ts

```ts
type Combinable = number | string;
type ConversionDescriptor = 'as-number' | 'as-text';
```

---

## 024: Type Aliases & Object Types

- we can also do it with objects

```ts
type User = { name: string; age: number };
const u1: User = { name: 'Max', age: 30 };
```

---

## Quiz 2: Core Types & Concepts

- done

---

## 025: Function Return Types & "void"

### app.ts

```ts
// best not to explicitly set return type
// let TS do its job
function add(n1: number, n2: number): number {
  return n1 + n2;
}

// this return type is `void`
// just means the function doesn't return anything
function printResult(num: number) {
  console.log('Result: ' + num)
}

printResult(add(5, 12)) // 17

let someValue: undefined; // is a valid type
// but a fuction is not allowed to return undefined
// void makes it clear that a function has no return value
```

---

## 026: Functions as Types

### app.ts

```ts
// let combineValues: Function;  // the function type

let combineValues: (a: number, b: number) => number; // accepts any function that takes two values and returns a number

combineValues = add;

console.log(combineValues(8,8)) // 16
```

---

## 027: Function Types & Callbacks

### app.ts

```ts
// calculate and pass result to cb function
function addAndHandle(n1: number, n2: number, cb: (num: number) => void) {
  const result = n1 + n2;
  cb(result);
}

printResult(add(5, 12));

let combineValues: (a: number, b: number) => number;

combineValues = add;
// combineValues = printResult;
// combineValues = 5;

console.log(combineValues(8, 8));

// let someValue: undefined;

// cb function simply logs the result
addAndHandle(10, 20, (result) => {
  console.log(result);
});
```

---

## Quiz 3: Functions & Types

- done

---

## 028: The "unknown" Type

```ts
let userInput: unknown;
let userName: string;

userInput = 5; // allowed
userInput = 'Max'; // allowed

// we can't say username = userInput without type checking
if (typeof userInput === 'string') {
  userName = userInput;
```

---

## 029: The "never" Type

```ts
// never = never returns a value; not just void, but never
function generateError(message: string, code: number): never {
  throw { message: message, errorCode: code };
  // while (true) {}
}

generateError('An error occurred!', 500);
```

---

## 030: Wrap Up

- done

---

## 031: Useful Resources & Links

[Official TypeScript Docs](https://www.typescriptlang.org/docs/handbook/basic-types.html)

---
